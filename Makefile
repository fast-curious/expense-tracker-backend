#* Variables
PYTHON := python
params := $(wordlist 2,100,$(MAKECMDGOALS))

#* Docker variables
IMAGE := expense_tracker
VERSION := latest

#* Directories with source code
CODE = expense_tracker tests
CODE_FMT = expense_tracker tests migrations

.DEFAULT: ;: do nothing

#* Poetry
.PHONY: poetry-download
poetry-download:
	curl -sSL https://install.python-poetry.org | $(PYTHON) -

.PHONY: poetry-remove
poetry-remove:
	curl -sSL https://install.python-poetry.org | $(PYTHON) - --uninstall

#* Installation
.PHONY: install
install:
	poetry install -n

.PHONY: pre-commit-install
pre-commit-install:
	poetry run pre-commit install

#* Formatters
.PHONY: codestyle
codestyle:
	poetry run pyupgrade --exit-zero-even-if-changed --py39-plus **/*.py
	poetry run autoflake --recursive --in-place --remove-all-unused-imports --ignore-init-module-imports $(CODE_FMT)
	poetry run isort --settings-path pyproject.toml $(CODE_FMT)
	poetry run black --config pyproject.toml $(CODE_FMT)

.PHONY: format
format: codestyle

#* Test
.PHONY: test-unit
test-unit:
	ENVIRONMENT=test poetry run pytest -c pyproject.toml -s tests/unit

.PHONY: test-int
test-int:
	ENVIRONMENT=test poetry run pytest -c pyproject.toml -s tests/integration --no-remote-runner

.PHONY: test-int-remote
test-int-remote:
	ENVIRONMENT=test poetry run pytest -c pyproject.toml -s tests/integration --remote-runner $(params)

.PHONY: test-load
test-load:
	poetry run locust -f tests/load_testing/locustfile.py --users 100 --spawn-rate 1

.PHONY: test-cov-report
test-cov-report:
	poetry run coverage xml

.PHONY: test
test: test-unit

.PHONY: fast-test
fast-test:
	ENVIRONMENT=test poetry run pytest -n 8 -c pyproject.toml -s tests/unit

# Validate pyproject.toml
.PHONY: check-poetry
check-poetry:
	poetry check

#* Check code style
.PHONY: check-isort
check-isort:
	poetry run isort --diff --check-only --settings-path pyproject.toml $(CODE)

.PHONY: check-black
check-black:
	poetry run black --diff --check --config pyproject.toml $(CODE)

.PHONY: check-darglint
check-darglint:
	poetry run darglint --verbosity 2 $(CODE)

.PHONY: check-codestyle
check-codestyle: check-isort check-black check-darglint

#* Static linters

.PHONY: check-pylint
check-pylint:
	poetry run pylint --rcfile=pyproject.toml $(CODE)

.PHONY: check-mypy
check-mypy:
	poetry run mypy --install-types --non-interactive --config-file pyproject.toml $(CODE)

.PHONY: static-lint
static-lint: check-pylint check-mypy

#* Check security issues

.PHONY: check-bandit
check-bandit:
	poetry run bandit -ll --recursive $(CODE)

.PHONY: check-safety
check-safety:
	poetry run safety check --full-report

.PHONY: check-security
check-security: check-safety check-bandit

.PHONY: lint
lint: check-poetry check-codestyle static-lint check-security

#* Docker
# Example: make docker VERSION=latest
# Example: make docker IMAGE=some_name VERSION=0.1.0
.PHONY: docker-build
docker-build:
	@echo Building docker $(IMAGE):$(VERSION) ...
	docker build \
		-t $(IMAGE):$(VERSION) . \
		-f ./docker/Dockerfile

# Example: make clean_docker VERSION=latest
# Example: make clean_docker IMAGE=some_name VERSION=0.1.0
.PHONY: docker-remove
docker-remove:
	@echo Removing docker $(IMAGE):$(VERSION) ...
	docker rmi -f $(IMAGE):$(VERSION)

#* Cleaning
.PHONY: pycache-remove
pycache-remove:
	find . | grep -E "(__pycache__|\.pyc|\.pyo$$)" | xargs rm -rf

	poetry install -n
.PHONY: build-remove
build-remove:
	rm -rf build/

.PHONY: dsstore-remove
dsstore-remove:
	find . | grep -E ".DS_Store" | xargs rm -rf

.PHONY: mypycache-remove
mypycache-remove:
	find . | grep -E ".mypy_cache" | xargs rm -rf

.PHONY: ipynbcheckpoints-remove
ipynbcheckpoints-remove:
	find . | grep -E ".ipynb_checkpoints" | xargs rm -rf

.PHONY: pytestcache-remove
pytestcache-remove:
	find . | grep -E ".pytest_cache" | xargs rm -rf

.PHONY: cleanup
cleanup: pycache-remove dsstore-remove mypycache-remove ipynbcheckpoints-remove pytestcache-remove

#* Local server run
.PHONY: server
server:
	uvicorn expense_tracker.main:app --reload

#* Handy commands
.PHONY: ci
ci: codestyle lint

.PHONY: migrate
migrate:
	alembic upgrade head

.PHONY: db
db:
	docker compose -f docker/docker-compose.yml up db
