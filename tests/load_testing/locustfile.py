from typing import Any, Optional

from locust import HttpUser, between, task

from expense_tracker.models import CurrencyCode, CurrencyDecimal, TransactionCreate
from tests.integration.steps import AuthSteps, TransactionSteps


class ExpenseTrackerUser(HttpUser):
    wait_time = between(3, 5)

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super(ExpenseTrackerUser, self).__init__(*args, **kwargs)

        self.auth_steps = AuthSteps(self.client)
        self.transaction_steps = TransactionSteps(self.client)

        self.created_transaction_id: Optional[int] = None

    def on_start(self) -> None:
        self.auth_steps.login_as_new_user()

    @task
    def read_currencies(self) -> None:
        self.client.get("/currencies")

    @task
    def create_rub_transaction(self) -> None:
        transaction_create = TransactionCreate(
            amount=CurrencyDecimal("13.37"),
            name="test transaction",
            comment="lol, it worked",
            currency_code=CurrencyCode("RUB"),
        )
        transaction = self.transaction_steps.create(transaction_create)

        self.created_transaction_id = transaction.id

    @task
    def read_particular_transaction(self) -> None:
        self.client.request_name = "/transactions/[id]"

        non_existent_transaction_id = -1
        target_transaction_id = (
            self.created_transaction_id or non_existent_transaction_id
        )
        with self.client.get(
            f"/transactions/{target_transaction_id}",
            catch_response=True,  # type: ignore
        ) as response:
            if (
                target_transaction_id == non_existent_transaction_id
                and response.status_code == 404
            ):
                response.success()  # type: ignore

    @task
    def read_page_of_transactions(self) -> None:
        self.transaction_steps.read_many()

    @task
    def read_docs(self) -> None:
        self.client.get("/docs")
