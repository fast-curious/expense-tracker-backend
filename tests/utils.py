from typing import Optional, TypeVar

from fastapi.testclient import TestClient
from pydantic import BaseModel
from requests import Response
from sqlalchemy import func
from sqlmodel import Session, SQLModel, select

from expense_tracker.models import UserCreate, Username
from expense_tracker.schemas.token import Token, TokenData
from expense_tracker.security import create_access_token, token_url


def generate_token_by_username(username: Username) -> Token:
    token_data = TokenData(username=username)
    access_token = create_access_token(token_data)
    return Token(access_token=access_token, token_type="bearer")


def get_authorization_header(token: Token) -> dict[str, str]:
    return {"Authorization": f"{token.token_type} {token.access_token}"}


def send_authentication_request(client: TestClient, user: UserCreate) -> Response:
    return client.post(
        f"/{token_url}",
        data=user.dict() | {"grant_type": "password"},
        headers={"content-type": "application/x-www-form-urlencoded"},
    )


M = TypeVar("M", bound=BaseModel)


def compare_models(
    received: M,
    expected: M,
    pk: str = "id",
    exclude_keys: Optional[set[str]] = None,
):
    if exclude_keys is None:
        exclude_keys = set()

    exclude_keys.add(pk)

    assert received.dict(exclude=exclude_keys) == expected.dict(exclude=exclude_keys)


S = TypeVar("S", bound=SQLModel)


def count_objects(session: Session, mapper: type[S]) -> int:
    count: Optional[int] = session.exec(select(func.count()).select_from(mapper)).first()  # type: ignore
    assert count is not None

    return count
