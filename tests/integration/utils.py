from contextlib import contextmanager

import pytest
from requests import HTTPError


@contextmanager
def response_code(status_code: int):
    with pytest.raises(HTTPError) as e:
        yield

    assert e.value.response.status_code == status_code
