from typing import Optional

from uuid import uuid4

from expense_tracker.schemas.token import Token

from .base import BaseSteps


def gen_uuid(length: int = 32) -> str:
    return str(uuid4())[:length]


class AuthSteps(BaseSteps):
    def register_user(
        self, username: Optional[str] = None, password: Optional[str] = None
    ) -> str:
        username = username or f"testuser-{gen_uuid()}"
        password = password or gen_uuid()

        r = self._client.post(
            "/user",
            json={
                "username": username,
                "password": password,
            },
        )

        r.raise_for_status()

        return username

    def login_user(
        self,
        username: str,
        password: str,
    ):
        r = self._client.post(
            "/token",
            data={
                "username": username,
                "password": password,
            },
        )
        r.raise_for_status()

        token = Token(**r.json())
        self._client.headers[
            "Authorization"
        ] = f"{token.token_type} {token.access_token}"

    def login_as_new_user(self) -> str:
        username = f"testuser-{gen_uuid()}"
        password = gen_uuid()

        self.register_user(username, password)
        self.login_user(username, password)

        return username
