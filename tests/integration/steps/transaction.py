from expense_tracker.models import TransactionCreate, TransactionRead, TransactionUpdate

from .base import CRUDSteps


class TransactionSteps(
    CRUDSteps[int, TransactionCreate, TransactionRead, TransactionUpdate]
):
    base_url = "/transactions"
    _read_schema = TransactionRead
