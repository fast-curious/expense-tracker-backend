from pydantic import parse_obj_as

from expense_tracker.models import Currency

from .base import BaseSteps


class CurrencySteps(BaseSteps):
    base_url = "/currencies"

    def list(self, limit: int = 30, offset: int = 0) -> list[Currency]:
        r = self._client.get(
            self.base_url,
            data={
                "limit": limit,
                "offset": offset,
            },
        )
        r.raise_for_status()

        return parse_obj_as(list[Currency], r.json())
