from expense_tracker.models import TagCreate, TagRead, TagUpdate
from tests.integration.steps.base import CRUDSteps


class TagSteps(CRUDSteps[int, TagCreate, TagRead, TagUpdate]):
    base_url = "/tags"
    _read_schema = TagRead
