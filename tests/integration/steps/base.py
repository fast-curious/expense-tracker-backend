from typing import Generic, TypeVar

from abc import ABC, abstractmethod

from pydantic import BaseModel, parse_obj_as
from requests import Session


class BaseSteps(ABC):
    def __init__(self, client: Session):
        self._client = client


IdType = TypeVar("IdType")
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
ReadSchemaType = TypeVar("ReadSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)


class CRUDSteps(
    Generic[IdType, CreateSchemaType, ReadSchemaType, UpdateSchemaType], BaseSteps
):
    @property
    @abstractmethod
    def base_url(self) -> str:
        """
        url for router. Must be with a trailing slash
        """

    @property
    @abstractmethod
    def _read_schema(self) -> type[ReadSchemaType]:
        pass

    def read(self, id_: IdType) -> ReadSchemaType:
        r = self._client.get(f"{self.base_url}/{id_}")
        r.raise_for_status()

        return self._read_schema(**r.json())

    def read_many(self, limit: int = 30, offset: int = 0) -> list[ReadSchemaType]:
        r = self._client.get(
            self.base_url,
            data={
                "limit": limit,
                "offset": offset,
            },
        )
        r.raise_for_status()

        return parse_obj_as(list[self._read_schema], r.json())  # type: ignore

    def create(self, payload: CreateSchemaType) -> ReadSchemaType:
        r = self._client.post(
            self.base_url,
            data=payload.json(),
        )
        r.raise_for_status()

        return self._read_schema(**r.json())

    def update(self, id_: IdType, payload: UpdateSchemaType) -> ReadSchemaType:
        r = self._client.patch(
            f"{self.base_url}/{id_}",
            data=payload.json(),
        )
        r.raise_for_status()

        return self._read_schema(**r.json())

    def delete(self, id_: IdType) -> ReadSchemaType:
        r = self._client.delete(f"{self.base_url}/{id_}")
        r.raise_for_status()

        return self._read_schema(**r.json())
