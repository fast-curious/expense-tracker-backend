from fastapi import status

from expense_tracker.models.tag import TagCreate, TagName, TagUpdate
from tests.integration.utils import response_code


def test_read_unauthenticated(tag_steps):
    with response_code(status.HTTP_401_UNAUTHORIZED):
        tag_steps.read(id_=1)


def test_create_tag(auth_steps, tag_steps):
    # prepare data
    tag_create = TagCreate(name=TagName("tagName"))

    # test body
    auth_steps.login_as_new_user()
    tag = tag_steps.create(tag_create)
    read_tag = tag_steps.read(tag.id)

    # check
    assert tag == read_tag
    assert tag.name == tag_create.name


def test_update_tag(auth_steps, tag_steps):
    # prepare data
    tag_create = TagCreate(name=TagName("tagName"))
    tag_update = TagUpdate(name=TagName("newTagName"))

    # test body
    auth_steps.login_as_new_user()
    tag = tag_steps.create(tag_create)
    updated_tag = tag_steps.update(tag.id, tag_update)

    # check
    assert updated_tag.name == tag_update.name


def test_delete_tag(auth_steps, tag_steps):
    # prepare data
    tag_create = TagCreate(name=TagName("tagName"))

    # test body
    auth_steps.login_as_new_user()
    tag = tag_steps.create(tag_create)
    tag_steps.delete(tag.id)

    # check
    with response_code(status.HTTP_404_NOT_FOUND):
        tag_steps.read(tag.id)
