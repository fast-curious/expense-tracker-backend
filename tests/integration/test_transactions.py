from fastapi import status

from expense_tracker.models import CurrencyCode, CurrencyDecimal, TransactionCreate
from tests.integration.utils import response_code


def test_read_unauthenticated(transaction_steps):
    with response_code(status.HTTP_401_UNAUTHORIZED):
        transaction_steps.read(id_=1)


def test_create_transaction(auth_steps, transaction_steps):
    auth_steps.login_as_new_user()
    transaction = transaction_steps.create(
        TransactionCreate(
            amount=CurrencyDecimal("13.37"),
            name="test transaction",
            comment="lol, it worked",
            currency_code=CurrencyCode("RUB"),
        ),
    )

    read_transaction = transaction_steps.read(transaction.id)

    assert transaction == read_transaction


def test_read_my_many_transactions(auth_steps, transaction_steps):

    # prepare data
    transaction_create = TransactionCreate(
        amount=CurrencyDecimal("13.37"),
        name="test transaction",
        comment="lol, it worked",
        currency_code=CurrencyCode("RUB"),
    )

    # test body
    auth_steps.login_as_new_user()
    transaction = transaction_steps.create(transaction_create)
    read_transactions = transaction_steps.read_many()

    # login as another user
    auth_steps.login_as_new_user()
    read_other_transactions = transaction_steps.read_many()

    # check
    assert transaction in read_transactions
    assert len(read_other_transactions) == 0


def test_delete_transaction(auth_steps, transaction_steps):

    # prepare data
    transaction_create = TransactionCreate(
        amount=CurrencyDecimal("13.37"),
        name="test transaction",
        comment="lol, it worked",
        currency_code=CurrencyCode("RUB"),
    )

    # test body
    auth_steps.login_as_new_user()
    transaction = transaction_steps.create(transaction_create)
    transaction_steps.delete(transaction.id)

    # check
    with response_code(status.HTTP_404_NOT_FOUND):
        transaction_steps.read(transaction.id)


def test_update_transaction(auth_steps, transaction_steps):

    # prepare data
    transaction_create = TransactionCreate(
        amount=CurrencyDecimal("13.37"),
        name="test transaction",
        comment="lol, it worked",
        currency_code=CurrencyCode("RUB"),
    )
    transaction_update = TransactionCreate(
        amount=CurrencyDecimal("20.45"),
        name="updated transaction",
        comment="transaction updated",
        currency_code=CurrencyCode("RUB"),
    )

    # test body
    auth_steps.login_as_new_user()
    transaction = transaction_steps.create(transaction_create)
    updated_transaction = transaction_steps.update(transaction.id, transaction_update)

    # check
    assert updated_transaction.currency.code == transaction_update.currency_code
