from typing import Iterable, Optional

import argparse
from urllib.parse import urljoin

import pytest
import requests
from fastapi.testclient import TestClient
from requests_toolbelt.sessions import BaseUrlSession

from tests.integration.steps import AuthSteps, CurrencySteps, TagSteps, TransactionSteps


def pytest_addoption(parser: pytest.Parser):
    parser.addoption(
        "--remote-runner", default=True, action=argparse.BooleanOptionalAction
    )
    parser.addoption("--remote-api-url", type=str)


class FixedBaseUrlSession(BaseUrlSession):
    def create_url(self, url):
        """Create the URL based off this partial path."""
        return urljoin(self.base_url, url.lstrip("/"))


@pytest.fixture(name="local_client_setup")
def local_client_setup_fixture(
    rub_currency, local_client: TestClient
) -> requests.Session:
    return local_client


@pytest.fixture(name="client")
def client_fixture(local_client_setup, request) -> Iterable[requests.Session]:
    is_running_remote: bool = request.config.getoption("--remote-runner")
    remote_url: Optional[str] = request.config.getoption("--remote-api-url")

    if not is_running_remote:
        yield local_client_setup
        return

    if remote_url is None:
        raise ValueError("Using remote client requires --remote-api-url")

    remote_client = FixedBaseUrlSession(base_url=remote_url)
    yield remote_client
    remote_client.close()
    return


@pytest.fixture(name="auth_steps")
def auth_steps_fixture(client) -> AuthSteps:
    return AuthSteps(client)


@pytest.fixture(name="transaction_steps")
def transaction_steps_fixture(client) -> TransactionSteps:
    return TransactionSteps(client)


@pytest.fixture(name="tag_steps")
def tag_steps_fixture(client) -> TagSteps:
    return TagSteps(client)


@pytest.fixture(name="currency_steps")
def currency_steps_fixture(client) -> CurrencySteps:
    return CurrencySteps(client)
