from expense_tracker.models import UserRead


def test_user_auth(auth_steps, client):
    username = auth_steps.login_as_new_user()

    r = client.get("user/me")
    assert r.status_code == 200

    user_data = UserRead(**r.json())
    assert user_data.username == username
