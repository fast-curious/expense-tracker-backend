import pytest
from fastapi import status
from fastapi.testclient import TestClient
from pydantic import parse_obj_as
from sqlmodel import Session

from expense_tracker.models import Tag, TagName, TagRead, User
from tests.utils import compare_models, count_objects


@pytest.fixture(name="multiple_tags")
def multiple_tags_fixture(session: Session, db_user: User):
    tags = []
    for idx in range(30):
        tag = Tag(name=TagName(f"Tag #{idx}"), user=db_user)
        tags.append(tag)
    session.add_all(tags)
    session.commit()

    return tags


def test_create_tag(authorized_client: TestClient):
    expected_tag = TagRead(id=666, name=TagName("foobar"))
    response = authorized_client.post("/tags", json={"name": "FooBar"})

    assert response.status_code == status.HTTP_200_OK
    created_tag = TagRead(**response.json())

    compare_models(created_tag, expected_tag)


def test_create_tag_unauthorized(
    client: TestClient,
    session: Session,
):
    response = client.post("/tags", json={"name": "sampleTag"})
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert count_objects(session, Tag) == 0


def test_read_tag(
    authorized_client: TestClient,
    db_tag: Tag,
):
    response = authorized_client.get(
        f"/tags/{db_tag.id}",
    )

    assert response.status_code == status.HTTP_200_OK
    read_tag = TagRead(**response.json())
    expected_tag = TagRead(**db_tag.dict())

    compare_models(read_tag, expected_tag)


def test_read_nonexistent_tag(
    authorized_client: TestClient,
):
    response = authorized_client.get(
        "/tags/999",
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_read_other_users_tag(
    other_authorized_client: TestClient,
    db_tag: Tag,
):
    response = other_authorized_client.get(
        f"/tags/{db_tag.id}",
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_read_tags(
    authorized_client: TestClient,
    db_tag: Tag,
):
    response = authorized_client.get(
        "/tags",
    )

    assert response.status_code == status.HTTP_200_OK

    read_tags = parse_obj_as(list[TagRead], response.json())
    expected_tags = [TagRead(**db_tag.dict())]
    assert read_tags == expected_tags


def test_read_tags_pagination(
    authorized_client: TestClient,
    multiple_tags: list[Tag],
):
    limit = 15
    offset = 10

    response = authorized_client.get(
        "/tags",
        params={
            "limit": limit,
            "offset": offset,
        },
    )
    assert response.status_code == status.HTTP_200_OK

    read_tags = parse_obj_as(list[TagRead], response.json())
    expected_tags = [
        TagRead(**tag.dict()) for tag in multiple_tags[offset : offset + limit]
    ]

    assert read_tags == expected_tags


def test_update_tag(db_tag: Tag, authorized_client: TestClient):
    new_name = TagName("lolkek")
    response = authorized_client.patch(f"/tags/{db_tag.id}", json={"name": new_name})

    assert response.status_code == status.HTTP_200_OK
    read_tag = TagRead(**response.json())

    expected_tag = TagRead(**db_tag.dict())
    expected_tag.name = new_name

    compare_models(read_tag, expected_tag)


def test_update_non_existent_tag(authorized_client: TestClient):
    new_name = "lolkek"
    response = authorized_client.patch("/tags/999", json={"name": new_name})

    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_update_other_user_tag(
    session: Session,
    db_tag: Tag,
    other_authorized_client: TestClient,
):
    new_name = "lolkek"
    response = other_authorized_client.patch(
        f"/tags/{db_tag.id}", json={"name": new_name}
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND
    existing_tag = TagRead(**db_tag.dict())

    session.refresh(db_tag)
    updated_tag = TagRead(**db_tag.dict())

    compare_models(updated_tag, existing_tag)


def test_update_tag_empty_diff(db_tag: Tag, authorized_client: TestClient):
    response = authorized_client.patch(
        f"/tags/{db_tag.id}",
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


def test_delete_tag(db_tag: Tag, authorized_client: TestClient, session: Session):
    response = authorized_client.delete(
        f"/tags/{db_tag.id}",
    )

    assert response.status_code == status.HTTP_200_OK
    assert count_objects(session, Tag) == 0


def test_delete_nonexistent_tag(authorized_client: TestClient, session: Session):
    response = authorized_client.delete(
        "/tags/999",
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_delete_other_user_tag(
    db_tag: Tag,
    other_authorized_client: TestClient,
    session: Session,
):
    response = other_authorized_client.delete(
        f"/tags/{db_tag.id}",
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert count_objects(session, Tag) == 1
