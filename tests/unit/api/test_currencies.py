import pytest
from fastapi import status
from fastapi.testclient import TestClient
from pydantic import parse_obj_as
from sqlmodel import Session

from expense_tracker.models import Currency


def test_read_currencies(
    session: Session, authorized_client: TestClient, many_currencies: list[Currency]
):
    """test that we can read list of currencies"""
    response = authorized_client.get("/currencies")
    assert response.status_code == status.HTTP_200_OK

    read_currencies = parse_obj_as(list[Currency], response.json())
    assert len(read_currencies) == 30
    assert read_currencies == many_currencies[:30]


@pytest.mark.usefixtures(
    "many_currencies",
)
def test_read_currencies_limit(session: Session, authorized_client: TestClient):
    """test that we can read list of currencies with limit"""
    limit = 2
    response = authorized_client.get("/currencies", params={"limit": limit})
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == limit
