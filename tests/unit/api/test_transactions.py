from typing import Any, NamedTuple, Optional

from datetime import datetime
from functools import partial

import pytest
from fastapi import status
from fastapi.testclient import TestClient
from sqlmodel import Session

from expense_tracker.models import Currency, Tag, TagRead, Transaction
from expense_tracker.models.transaction import CurrencyDecimal, TransactionRead
from tests.utils import compare_models, count_objects

compare_transactions = partial(compare_models, exclude_keys={"created_at"})


class TransactionJson(NamedTuple):
    amount: Optional[CurrencyDecimal]
    name: Optional[str]
    comment: Optional[str]
    currency_code: Optional[str]


@pytest.fixture(name="minimal_transaction_create")
def minimal_transaction_create_fixture() -> dict[str, Any]:
    return {
        "amount": 150,
        "name": "random",
        "currency_code": "rub",
    }


@pytest.mark.usefixtures("rub_currency")
def test_create_minimal_transaction(session: Session, authorized_client: TestClient):
    response = authorized_client.post(
        "/transactions",
        json={
            "amount": 150,
            "name": "Food",
            "currency_code": "rub",
        },
    )
    data = response.json()

    assert response.status_code == 200
    assert data["amount"] == 150
    assert data["name"] == "Food"
    assert data["currency"]["code"] == "rub"
    assert data["base_currency"]["code"] == "rub"
    assert data["base_amount"] == 150


@pytest.mark.usefixtures("rub_currency", "usd_currency", "usd_exchange_rate")
def test_create_usd_transaction(session: Session, authorized_client: TestClient):
    """Test than we can create transaction in USD."""
    response = authorized_client.post(
        "/transactions",
        json={
            "amount": 5,
            "name": "Food",
            "currency_code": "usd",
        },
    )
    data = response.json()

    assert response.status_code == 200
    assert data["amount"] == 5
    assert data["name"] == "Food"
    assert data["currency"]["code"] == "usd"
    assert data["base_currency"]["code"] == "rub"
    assert data["base_amount"] == 350  # rate: 70


@pytest.mark.usefixtures("rub_currency", "usd_currency")
def test_create_usd_transaction_no_rate(
    session: Session, authorized_client: TestClient
):
    """
    Test that we can create transaction in USD
    with no exchange rate in database.
    """
    response = authorized_client.post(
        "/transactions",
        json={
            "amount": 5,
            "name": "Food",
            "currency_code": "usd",
        },
    )
    data = response.json()

    assert response.status_code == 200
    assert data["amount"] == 5
    assert data["name"] == "Food"
    assert data["currency"]["code"] == "usd"
    assert data["base_currency"]["code"] == "rub"
    assert data["base_amount"] is None


@pytest.mark.usefixtures("rub_currency")
def test_create_transaction_with_comment(
    session: Session,
    authorized_client: TestClient,
    minimal_transaction_create: dict[str, Any],
    rub_currency: Currency,
    base_currency: Currency,
):
    """Test that we can create transaction with a comment."""
    minimal_transaction_create["comment"] = "foobar"

    response = authorized_client.post(
        "/transactions",
        json=minimal_transaction_create,
    )
    assert response.status_code == status.HTTP_200_OK

    read_object = TransactionRead(**response.json())

    expected_response = TransactionRead(
        id=1,
        created_at=datetime.now(),
        currency=rub_currency,
        base_currency=base_currency,
        base_amount=minimal_transaction_create["amount"],
        **minimal_transaction_create,
    )
    compare_transactions(read_object, expected_response)


def test_create_transaction_with_tag(
    authorized_client: TestClient,
    db_tag: Tag,
    minimal_transaction_create: dict[str, Any],
    rub_currency: Currency,
    base_currency: Currency,
):
    """Test that we can create transaction with a tag."""
    payload = minimal_transaction_create.copy()
    payload["tag_id"] = db_tag.id

    response = authorized_client.post(
        "/transactions",
        json=payload,
    )
    assert response.status_code == status.HTTP_200_OK

    read_object = TransactionRead(**response.json())

    expected_response = TransactionRead(
        id=1,
        created_at=datetime.now(),
        currency=rub_currency,
        base_currency=base_currency,
        base_amount=minimal_transaction_create["amount"],
        tag=TagRead(**db_tag.dict()),
        **minimal_transaction_create,
    )

    compare_transactions(read_object, expected_response)


def test_create_transaction_with_nonexistent_tag(
    session: Session,
    authorized_client: TestClient,
    minimal_transaction_create: dict[str, Any],
    rub_currency: Currency,
):
    """Test that we can create transaction with a non-existent tag."""
    payload = minimal_transaction_create.copy()
    payload["tag_id"] = 1

    response = authorized_client.post(
        "/transactions",
        json=payload,
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert count_objects(session, Transaction) == 0


def test_create_transaction_with_other_user_tag(
    session: Session,
    other_authorized_client: TestClient,
    db_tag: Tag,
    minimal_transaction_create: dict[str, Any],
    rub_currency: Currency,
):
    """Test that we can create transaction with a tag of another user."""
    payload = minimal_transaction_create.copy()
    payload["tag_id"] = db_tag.id

    response = other_authorized_client.post(
        "/transactions",
        json=payload,
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert count_objects(session, Transaction) == 0


@pytest.mark.usefixtures("rub_currency")
@pytest.mark.parametrize(
    "transaction_json",
    [
        {
            "amount": 150,
            "comment": "some comment",
            "currency_code": "rub",
        },
        {
            "name": "random",
            "comment": "some comment",
            "currency_code": "rub",
        },
        {"amount": 150, "name": "random", "comment": "some comment"},
    ],
)
def test_create_transaction_with_missing_field(
    session: Session, authorized_client: TestClient, transaction_json: TransactionJson
):
    """Test that we cannot create transaction with missing name, amount, or currency code."""
    response = authorized_client.post(
        "/transactions",
        json=transaction_json,
    )
    assert response.status_code == 422


def test_unauthorizated_create_transaction(
    session: Session,
    client: TestClient,
    minimal_transaction_create: dict[str, Any],
):
    """Test that that unauthorized users cannot create transactions."""
    response = client.post(
        "/transactions",
        json=minimal_transaction_create,
    )
    assert response.status_code == 401


@pytest.mark.usefixtures("rub_currency")
def test_read_transaction(
    session: Session, authorized_client: TestClient, db_transaction: Transaction
):
    """Test that we can read a transaction."""
    response = authorized_client.get(f"/transactions/{db_transaction.id}")
    assert response.status_code == 200
    assert response.json()["id"] == db_transaction.id


@pytest.mark.usefixtures("rub_currency")
def test_update_transaction_value(
    session: Session,
    authorized_client: TestClient,
    db_transaction: Transaction,
    minimal_transaction_create: dict[str, Any],
):
    """Test that we can update value of transaction."""
    new_amount_value = int(db_transaction.amount + 1)
    response = authorized_client.patch(
        f"/transactions/{db_transaction.id}",
        json={"amount": new_amount_value},
    )
    assert response.status_code == 200
    assert response.json()["amount"] == new_amount_value
    assert response.json()["base_amount"] == new_amount_value


@pytest.mark.usefixtures("rub_currency", "usd_currency", "usd_exchange_rate")
def test_update_transaction_value_and_currency(
    session: Session,
    authorized_client: TestClient,
    db_transaction: Transaction,
    minimal_transaction_create: dict[str, Any],
):
    """Test that we can update value and currency of transaction."""
    new_amount_value = 5
    response = authorized_client.patch(
        f"/transactions/{db_transaction.id}",
        json={"amount": new_amount_value, "currency_code": "usd"},
    )
    assert response.status_code == 200
    assert response.json()["amount"] == 5
    assert response.json()["base_amount"] == 350  # rate: 70


@pytest.mark.usefixtures("rub_currency")
def test_delete_transaction(
    session: Session, authorized_client: TestClient, db_transaction: Transaction
):
    """Test that we can delete a transaction."""
    response = authorized_client.delete(f"/transactions/{db_transaction.id}")
    assert response.status_code == 200
    # verify deletion
    response = authorized_client.get(f"/transactions/{db_transaction.id}")
    assert response.status_code == 404
