import pytest
from fastapi import HTTPException, status
from fastapi.testclient import TestClient
from sqlmodel import Session

from expense_tracker.models import User, UserCreate
from expense_tracker.schemas.token import Token
from expense_tracker.security import get_current_user, hash_password, verify_password
from tests.utils import generate_token_by_username, send_authentication_request


@pytest.fixture()
def valid_pass_hash_pair():
    passwd = "my_very_secret_pass123456"
    return passwd, hash_password(passwd)


@pytest.fixture()
def invalid_pass_hash_pair(valid_pass_hash_pair):
    passwd, _ = valid_pass_hash_pair
    return passwd, hash_password(passwd[:-1])


@pytest.fixture()
def user_token(user_create: UserCreate) -> Token:
    return generate_token_by_username(user_create.username)


@pytest.fixture()
def malformed_token(user_token) -> Token:
    _, token = user_token
    token.access_token = "fakeFakeTokenStr"
    return token


def test_hash_and_verify(valid_pass_hash_pair):
    passwd, passwd_hash = valid_pass_hash_pair
    assert verify_password(passwd, passwd_hash)


def test_invalid_hash(invalid_pass_hash_pair):
    passwd, passwd_hash_invalid = invalid_pass_hash_pair
    assert not verify_password(passwd, passwd_hash_invalid)


def test_authenticate(client: TestClient, db_user: User, user_create: UserCreate):
    rv = send_authentication_request(client, user_create)

    assert rv.status_code == status.HTTP_200_OK
    Token.parse_raw(rv.text)


def test_authenticate_fail_password(
    client: TestClient, db_user: User, user_create: UserCreate
):
    user_create_wrong = UserCreate(
        username=user_create.username, password=user_create.password[:-1]
    )
    rv = send_authentication_request(client, user_create_wrong)
    assert rv.status_code == status.HTTP_401_UNAUTHORIZED


def test_authenticate_fail_no_user(client: TestClient, user_create):
    rv = send_authentication_request(client, user_create)
    assert rv.status_code == status.HTTP_401_UNAUTHORIZED


def test_get_current_user(session: Session, db_user: User, user_token: Token):
    user = get_current_user(session, user_token.access_token)
    assert user.username == db_user.username


def test_get_current_user_malformed_token(session: Session, user_token: Token):
    with pytest.raises(HTTPException):
        get_current_user(session, user_token.access_token)
