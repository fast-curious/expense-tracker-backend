from typing import Iterator

import random

import pytest
from fastapi.testclient import TestClient
from requests.models import CaseInsensitiveDict
from sqlmodel import Session

from expense_tracker.config import config
from expense_tracker.models import (
    Currency,
    CurrencyCode,
    Tag,
    TagName,
    Transaction,
    User,
    UserCreate,
    Username,
)
from expense_tracker.models.transaction import CurrencyDecimal
from expense_tracker.schemas.token import Token
from expense_tracker.security import hash_password
from tests.utils import generate_token_by_username, get_authorization_header


@pytest.fixture(autouse=True)
def no_http_requests(monkeypatch):
    """Disable network requests in unit tests"""

    def urlopen_mock(self, method, url, *args, **kwargs):
        raise RuntimeError(
            f"The test was about to {method} {self.scheme}://{self.host}{url}"
        )

    monkeypatch.setattr(
        "urllib3.connectionpool.HTTPConnectionPool.urlopen", urlopen_mock
    )


@pytest.fixture(name="client")
def client_fixture(local_client) -> Iterator[TestClient]:
    return local_client


@pytest.fixture(name="user_create")
def user_create_fixture(session: Session) -> UserCreate:
    return UserCreate(username=Username("username"), password="password")


@pytest.fixture(name="db_user")
def db_user_fixture(user_create: UserCreate, session: Session) -> User:
    hashed_password = hash_password(user_create.password)
    user = User(username=user_create.username, password_hash=hashed_password)
    session.add(user)
    session.commit()
    return user


@pytest.fixture(name="other_db_user")
def other_db_user_fixture(user_create: UserCreate, session: Session) -> User:
    user_create = UserCreate(username=Username("other_username"), password="password")
    hashed_password = hash_password(user_create.password)
    user = User(username=user_create.username, password_hash=hashed_password)
    session.add(user)
    session.commit()
    return user


@pytest.fixture(name="user_token")
def user_token_fixture(db_user: User) -> Token:
    return generate_token_by_username(username=db_user.username)


@pytest.fixture(name="authorized_client")
def authorized_client_fixture(client: TestClient, user_token: Token) -> TestClient:
    client.headers = CaseInsensitiveDict(get_authorization_header(user_token))
    return client


@pytest.fixture(name="other_authorized_client")
def other_authorized_client_fixture(client: TestClient, other_db_user: User):
    other_user_token = generate_token_by_username(other_db_user.username)
    client.headers = CaseInsensitiveDict(get_authorization_header(other_user_token))
    return client


@pytest.fixture(name="db_transaction")
def db_transaction_fixture(
    db_user: User, session: Session, rub_currency: Currency, base_currency: Currency
) -> Transaction:
    amount = CurrencyDecimal(random.randint(0, 10**3))
    transaction = Transaction(
        amount=amount,
        username=f"transaction_{random.randint(0, 10 ** 5)}",
        name=f"transaction_{random.randint(0, 10 ** 5)}",
        user=db_user,
        currency=rub_currency,
        base_currency=base_currency,
        base_amount=amount,
    )
    session.add(transaction)
    session.commit()
    return transaction


@pytest.fixture(name="db_tag")
def db_tag_fixture(session: Session, db_user: User) -> Tag:
    tag = Tag(name=TagName("test tag"), user=db_user)
    session.add(tag)
    session.commit()
    session.refresh(tag)

    return tag


@pytest.fixture(name="many_currencies")
def many_currency_fixture(session: Session) -> list[Currency]:
    currencies = []
    for i in range(100, 164):
        currency = Currency(code=CurrencyCode(i), name=f"Russian ruble #{i}")
        currencies.append(currency)
    session.add_all(currencies)
    session.commit()
    return currencies


@pytest.fixture(name="base_currency")
def base_currency_fixture(session: Session) -> Currency:
    base_currency = session.get(Currency, config.base_currency.code)
    if not base_currency:
        base_currency = Currency(
            code=config.base_currency.code,
            name=config.base_currency.name,
        )
        session.add(base_currency)
        session.commit()
    return base_currency
