from decimal import Decimal

import pytest

from expense_tracker.services.cbr import CBRClient, CBRResponseError, DailyRatesCBR


def test_cbr_models(cbr_response: str):
    DailyRatesCBR.parse_raw(cbr_response)


@pytest.mark.usefixtures("mocked_cbr")
def test_cbr_client():
    client = CBRClient()
    cbr_rates = client.get_currency_rates()
    assert "USD" in cbr_rates.currency_rates
    assert cbr_rates.currency_rates["USD"].char_code == "USD"
    assert cbr_rates.currency_rates["USD"].value == Decimal("74.8501")


def test_cbr_internal_error(requests_mock):
    requests_mock.get(CBRClient.CBR_DAILY_URL, status_code=500)
    client = CBRClient()
    with pytest.raises(CBRResponseError):
        client.get_currency_rates()
