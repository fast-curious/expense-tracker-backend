from decimal import Decimal

import pytest
from sqlmodel import Session, select

from expense_tracker.config import config
from expense_tracker.models import Currency, CurrencyCode, CurrencyDecimal, ExchangeRate
from expense_tracker.services.exchange_rate import (
    add_base_currency,
    get_amount_in_base_currency,
    populate_currencies_from_cbr,
    update_exchange_rates,
)


def test_add_base_currency(session: Session):
    add_base_currency(session)
    currency = session.get(Currency, config.base_currency.code)
    assert currency is not None
    assert currency.name == config.base_currency.name
    assert currency.code == config.base_currency.code


@pytest.mark.usefixtures("mocked_cbr")
def test_populate_currencies_from_cbr(session: Session):
    populate_currencies_from_cbr(session)
    currencies = session.exec(select(Currency)).all()
    assert Currency(code=CurrencyCode("usd"), name="Доллар США") in currencies
    assert Currency(code=CurrencyCode("eur"), name="Евро") in currencies


@pytest.mark.usefixtures("prefilled_currencies")
def test_update_exchange_rates(session: Session):
    update_exchange_rates(session)
    exchange_rate = session.exec(
        select(ExchangeRate).where(
            ExchangeRate.currency_code == "usd",
            ExchangeRate.base_currency_code == "rub",
        )
    ).first()
    assert exchange_rate is not None
    assert exchange_rate.nominal == 1
    assert exchange_rate.value == Decimal("74.8501")


@pytest.mark.usefixtures("mocked_cbr", "prefilled_exchange_rates")
def test_get_amount_in_base_currency(session: Session):
    amount = get_amount_in_base_currency(
        session, CurrencyDecimal("2.5"), CurrencyCode("usd")
    )
    assert amount == CurrencyDecimal("187.12525")


def test_get_amount_in_base_currency_for_base_currency(session: Session):
    amount = get_amount_in_base_currency(
        session, CurrencyDecimal(1), CurrencyCode("rub")
    )
    assert amount == CurrencyDecimal(1)
