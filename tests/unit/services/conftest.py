from pathlib import Path

import pytest
from sqlmodel import Session

from expense_tracker.services.cbr import CBRClient
from expense_tracker.services.exchange_rate import (
    populate_currencies_from_cbr,
    update_exchange_rates,
)


@pytest.fixture
def cbr_response() -> str:
    with open(Path(__file__).parent / "static" / "cbr.json", "r") as f:
        return f.read()


@pytest.fixture
def mocked_cbr(cbr_response: str, requests_mock):
    requests_mock.get(CBRClient.CBR_DAILY_URL, text=cbr_response)


@pytest.fixture
def prefilled_currencies(session: Session, mocked_cbr, base_currency):
    populate_currencies_from_cbr(session)


@pytest.fixture
def prefilled_exchange_rates(session: Session, prefilled_currencies):
    update_exchange_rates(session)
