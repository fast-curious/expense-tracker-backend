from typing import Iterator

import pytest
from fastapi.testclient import TestClient
from sqlmodel import Session, SQLModel, create_engine
from sqlmodel.pool import StaticPool

from expense_tracker.db import get_session
from expense_tracker.main import app
from expense_tracker.models import Currency, CurrencyCode, CurrencyDecimal, ExchangeRate


@pytest.fixture(name="session")
def session_fixture() -> Iterator[Session]:
    engine = create_engine(
        "sqlite://", connect_args={"check_same_thread": False}, poolclass=StaticPool
    )
    SQLModel.metadata.create_all(engine)
    with Session(engine) as session:
        yield session


@pytest.fixture(name="rub_currency")
def rub_currency_fixture(session: Session):
    currency = Currency(code=CurrencyCode("rub"), name="Russian ruble")
    session.add(currency)
    session.commit()
    return currency


@pytest.fixture(name="usd_currency")
def usd_currency_fixture(session: Session):
    currency = Currency(code=CurrencyCode("usd"), name="United States Dollar")
    session.add(currency)
    session.commit()
    return currency


@pytest.fixture(name="usd_exchange_rate")
def usd_exchange_rate_fixture(
    session: Session, rub_currency: Currency, usd_currency: Currency
):
    exchange_rate = ExchangeRate(
        currency=usd_currency, base_currency=rub_currency, value=CurrencyDecimal(70)
    )
    session.add(exchange_rate)
    session.commit()
    return exchange_rate


@pytest.fixture(name="local_client")
def local_client_fixture(session: Session) -> Iterator[TestClient]:
    def get_session_override() -> Session:
        return session

    app.dependency_overrides[get_session] = get_session_override
    client = TestClient(app)
    yield client
    app.dependency_overrides.clear()
