"""add currencies

Revision ID: f9ebd9e3cd94
Revises: fe12342eae9e
Create Date: 2022-03-07 15:28:38.912195

"""
import sqlalchemy as sa
from alembic import context, op

# revision identifiers, used by Alembic.
revision = "f9ebd9e3cd94"
down_revision = "fe12342eae9e"
branch_labels = None
depends_on = None


def upgrade():
    schema_upgrades()
    if context.get_x_argument(as_dictionary=True).get("data", None):
        data_upgrades()


def downgrade():
    if context.get_x_argument(as_dictionary=True).get("data", None):
        data_downgrades()
    schema_downgrades()


def schema_upgrades():
    """schema upgrade migrations go here."""


def schema_downgrades():
    """schema downgrade migrations go here."""


def data_upgrades():
    """Add any optional data upgrade migrations here!"""
    my_table = sa.table(
        "currency",
        sa.column("code", sa.String),
        sa.column("name", sa.String),
    )

    op.bulk_insert(
        my_table,
        [
            {"code": "rub", "name": "Russian Ruble"},
            {"code": "usd", "name": "US Dollar"},
            {"code": "kzt", "name": "Kazakhstani Tenge"},
        ],
    )


def data_downgrades():
    """Add any optional data downgrade migrations here!"""
    op.execute("delete from currency where code in ('rub', 'usd', 'kzt')")
