"""Add amount in base currency for transactions

Revision ID: b458a25e6807
Revises: 06e7ddcffcb7
Create Date: 2022-05-09 23:37:10.881893

"""
import sqlalchemy as sa
import sqlmodel
from alembic import context, op

# revision identifiers, used by Alembic.
revision = "b458a25e6807"
down_revision = "06e7ddcffcb7"
branch_labels = None
depends_on = None


def upgrade():
    schema_upgrades()
    if context.get_x_argument(as_dictionary=True).get("data", None):
        data_upgrades()


def downgrade():
    if context.get_x_argument(as_dictionary=True).get("data", None):
        data_downgrades()
    schema_downgrades()


def schema_upgrades():
    """schema upgrade migrations go here."""
    op.add_column(
        "transaction",
        sa.Column("base_amount", sa.Numeric(precision=15, scale=4), nullable=True),
    )
    op.add_column(
        "transaction",
        sa.Column(
            "base_currency_code", sqlmodel.sql.sqltypes.AutoString(), nullable=False, server_default='rub'
        ),
    )
    op.alter_column('transaction', 'base_currency_code', server_default=None)
    op.create_foreign_key(
        None, "transaction", "currency", ["base_currency_code"], ["code"]
    )


def schema_downgrades():
    """schema downgrade migrations go here."""
    op.drop_constraint(None, "transaction", type_="foreignkey")
    op.drop_column("transaction", "base_currency_code")
    op.drop_column("transaction", "base_amount")


def data_upgrades():
    """Add any optional data upgrade migrations here!"""


def data_downgrades():
    """Add any optional data downgrade migrations here!"""
