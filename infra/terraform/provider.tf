terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.71.0"
    }
  }
}

// Set variables in *.tfvars file
variable "yandex_oauth" {}
variable "yandex_cloud_id" {}
variable "yandex_folder_id" {}

provider "yandex" {
  token     = var.yandex_oauth
  cloud_id  = var.yandex_cloud_id
  folder_id = var.yandex_folder_id
  zone      = "ru-central1-a"
}
