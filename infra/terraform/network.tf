resource "yandex_vpc_network" "main-net" {
  name = "main-net"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name       = "subnet1"
  zone       = "ru-central1-a"
  network_id = yandex_vpc_network.main-net.id
  v4_cidr_blocks = [
    "192.168.10.0/24",
  ]
}

resource "yandex_vpc_subnet" "stage-net" {
  name       = "stage-net"
  zone       = "ru-central1-a"
  network_id = yandex_vpc_network.main-net.id
  v4_cidr_blocks = [
    "192.168.11.0/24",
  ]
}
