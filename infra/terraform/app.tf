variable "provisioner_private_key_path" {}

resource "yandex_compute_instance" "main" {
  name        = "expense-tracker-main-serv"
  platform_id = "standard-v3"
  hostname    = "backend-main"

  boot_disk {
    initialize_params {
      // Ubuntu 20.04
      image_id = "fd8mfc6omiki5govl68h"
      size     = 50
      // Gb
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  resources {
    cores         = 2
    core_fraction = 20
    // % guaranteed CPU
    memory = 1
  }

  metadata = {
    // cloud-init не обновляет инфу о пользователях. Она задается при создании и все
    user-data = file("cloud_config/main-meta.yml")
  }

  provisioner "remote-exec" {
    inline = [
      "echo connected!"
    ]

    connection {
      host        = self.network_interface.0.nat_ip_address
      type        = "ssh"
      user        = "provisioner"
      private_key = file(var.provisioner_private_key_path)
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u provisioner -i '${self.network_interface.0.nat_ip_address},' --private-key ${var.provisioner_private_key_path} --vault-password-file ./.vault_pass.txt ../ansible/provision.yml"
  }
}


resource "yandex_compute_instance" "staging" {
  name        = "expense-tracker-stage-serv"
  platform_id = "standard-v3"
  hostname    = "backend-stage"

  boot_disk {
    initialize_params {
      // Ubuntu 20.04
      image_id = "fd8mfc6omiki5govl68h"
      size     = 50
      // Gb
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.stage-net.id
    nat       = true
  }
  resources {
    cores         = 2
    core_fraction = 20
    // % guaranteed CPU
    memory = 1
  }

  metadata = {
    // cloud-init не обновляет инфу о пользователях. Она задается при создании и все
    user-data = file("cloud_config/integration-meta.yml")
  }

  #  provisioner "remote-exec" {
  #    inline = [
  #      "echo connected!"
  #    ]
  #
  #    connection {
  #      host        = self.network_interface.0.nat_ip_address
  #      type        = "ssh"
  #      user        = "provisioner"
  #      private_key = file(var.provisioner_private_key_path)
  #    }
  #  }
  #
  #  provisioner "local-exec" {
  #    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u provisioner -i '${self.network_interface.0.nat_ip_address},' --private-key ${var.provisioner_private_key_path} --vault-password-file ./.vault_pass.txt ../ansible/provision.yml"
  #  }
}
