resource "yandex_dns_zone" "zone1" {
  name   = "expense-tracker-dns"
  zone   = "mexator.xyz."
  public = true
}

resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "expense-tracker.mexator.xyz."
  type    = "A"
  ttl     = 200
  data = [
    yandex_compute_instance.main.network_interface.0.nat_ip_address
  ]
}

resource "yandex_dns_recordset" "rs2" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "stage.expense-tracker.mexator.xyz."
  type    = "A"
  ttl     = 200
  data = [
    yandex_compute_instance.staging.network_interface.0.nat_ip_address
  ]
}
