from typing import Any, Callable, TypeVar, cast

import time
from functools import wraps

from loguru import logger

F = TypeVar("F", bound=Callable[..., Any])


def log_task_enter_and_exit(func: F) -> F:
    @wraps(func)
    def wrapped(*args: Any, **kwargs: Any) -> Any:
        start = time.time()
        logger.info("Starting task '{}'", func.__name__)
        end = time.time()
        result = func(*args, **kwargs)
        logger.info("Task '{}' finished in {:f} s", func.__name__, end - start)
        return result

    return cast(F, wrapped)
