from loguru import logger
from sqlmodel import Session

from expense_tracker.db import engine
from expense_tracker.services.exchange_rate import (
    populate_currencies_from_cbr,
    update_exchange_rates,
)
from expense_tracker.tasks.utils import log_task_enter_and_exit


@logger.catch
@log_task_enter_and_exit
def populate_currencies_from_cbr_task() -> None:
    with Session(engine) as session:
        populate_currencies_from_cbr(session)


@logger.catch
@log_task_enter_and_exit
def update_exchange_rates_task() -> None:
    with Session(engine) as session:
        update_exchange_rates(session)
