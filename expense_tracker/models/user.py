from pydantic import BaseModel, ConstrainedStr
from sqlmodel import Field, SQLModel


class Username(ConstrainedStr):
    min_length = 8
    max_length = 50


class User(SQLModel, table=True):
    username: Username = Field(primary_key=True)
    password_hash: str


class UserRead(BaseModel):
    username: Username

    class Config:
        orm_mode = True


class UserCreate(BaseModel):
    username: Username
    password: str

    class Config:
        orm_mode = True
