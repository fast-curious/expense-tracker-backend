from datetime import datetime

from sqlalchemy import PrimaryKeyConstraint
from sqlmodel import Field, Relationship, SQLModel

from .currency import Currency
from .transaction import CurrencyDecimal


class ExchangeRate(SQLModel, table=True):
    currency_code: str = Field(foreign_key="currency.code")
    currency: Currency = Relationship(
        sa_relationship_kwargs={"foreign_keys": "ExchangeRate.currency_code"}
    )
    base_currency_code: str = Field(foreign_key="currency.code")
    base_currency: Currency = Relationship(
        sa_relationship_kwargs={"foreign_keys": "ExchangeRate.base_currency_code"}
    )
    nominal: int = Field(default=1, nullable=False)
    value: CurrencyDecimal
    created_at: datetime = Field(default_factory=datetime.utcnow, nullable=False)
    updated_at: datetime = Field(default_factory=datetime.utcnow, nullable=False)

    __table_args__ = (PrimaryKeyConstraint("currency_code", "base_currency_code"),)
