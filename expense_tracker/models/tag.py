from typing import Optional

from pydantic import ConstrainedStr
from sqlmodel import Field, Relationship, SQLModel

from .base import UpdateModel
from .user import User

__all__ = [
    "TagName",
    "Tag",
    "TagCreate",
    "TagRead",
    "TagUpdate",
]


class TagName(ConstrainedStr):
    to_lower = True
    strip_whitespace = True
    min_length = 1
    max_length = 32


class TagBase(SQLModel):
    name: TagName


class Tag(TagBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True, nullable=False)

    username: str = Field(foreign_key="user.username", index=True)
    user: User = Relationship()


class TagCreate(TagBase):
    pass


class TagRead(TagBase):
    id: int


class TagUpdate(UpdateModel):
    name: Optional[TagName] = None
