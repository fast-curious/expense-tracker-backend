from typing import Any

from pydantic import root_validator
from sqlmodel import SQLModel


class UpdateModel(SQLModel):
    @root_validator(allow_reuse=True)
    def any_present(cls, values: dict[str, Any]) -> dict[str, Any]:
        any_value_present = any(value is not None for value in values.values())
        if not any_value_present:
            raise ValueError("Update model must supply at least one value")
        return values
