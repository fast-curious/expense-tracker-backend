# pylint: disable=unexpected-keyword-arg
from typing import Optional

from datetime import datetime

from pydantic import ConstrainedDecimal
from sqlmodel import Field, Relationship, SQLModel

from .base import UpdateModel
from .currency import Currency, CurrencyCode
from .tag import Tag, TagRead
from .user import User

__all__ = [
    "Transaction",
    "TransactionCreate",
    "TransactionRead",
    "TransactionUpdate",
    "CurrencyDecimal",
]


class CurrencyDecimal(ConstrainedDecimal):
    max_digits = 15
    decimal_places = 4


class TransactionBase(SQLModel):
    amount: CurrencyDecimal
    name: str = Field(max_length=64)
    comment: Optional[str] = Field(default=None, max_length=512)


class Transaction(TransactionBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True, nullable=False)
    created_at: datetime = Field(default_factory=datetime.utcnow, index=True)

    username: str = Field(foreign_key="user.username", index=True)
    user: User = Relationship()

    base_amount: Optional[CurrencyDecimal]
    base_currency_code: str = Field(foreign_key="currency.code")
    base_currency: Currency = Relationship(
        sa_relationship_kwargs={
            "foreign_keys": "Transaction.base_currency_code",
            "lazy": "joined",
        }
    )

    currency_code: CurrencyCode = Field(foreign_key="currency.code")
    currency: Currency = Relationship(
        sa_relationship_kwargs={
            "foreign_keys": "Transaction.currency_code",
            "lazy": "joined",
        }
    )

    tag_id: Optional[int] = Field(foreign_key="tag.id", index=True)
    tag: Optional[Tag] = Relationship(sa_relationship_kwargs={"lazy": "joined"})


class TransactionCreate(TransactionBase):
    currency_code: CurrencyCode
    tag_id: Optional[int] = None


class TransactionRead(TransactionBase):
    id: int
    created_at: datetime
    currency: Currency
    base_currency: Currency
    base_amount: Optional[CurrencyDecimal]
    tag: Optional[TagRead] = None


class TransactionUpdate(UpdateModel):
    currency_code: Optional[CurrencyCode] = None
    amount: Optional[CurrencyDecimal] = None
    name: Optional[str] = Field(default=None, max_length=64)
    comment: Optional[str] = Field(default=None, max_length=512)
    tag_id: Optional[int] = None
