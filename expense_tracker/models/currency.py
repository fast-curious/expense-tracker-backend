from typing import Optional

from pydantic import ConstrainedStr
from sqlmodel import Field, SQLModel

__all__ = [
    "CurrencyCode",
    "Currency",
]


class CurrencyCode(ConstrainedStr):
    to_lower = True
    min_length = 3
    max_length = 3


class Currency(SQLModel, table=True):
    code: CurrencyCode = Field(primary_key=True, nullable=False)
    name: Optional[str] = Field(default=None, max_length=64)
