import os

from pydantic import BaseSettings, PositiveInt

from expense_tracker.models import CurrencyCode


class PostgresSettings(BaseSettings):
    """Settings for PostgreSQL database."""

    driver: str
    host: str
    port: int
    db: str
    user: str
    password: str

    @property
    def dsn(self) -> str:
        return f"{self.driver}://{self.user}:{self.password}@{self.host}:{self.port}/{self.db}"

    class Config:
        env_prefix = "postgres_"
        case_sensitive = False


class BaseCurrencySettings(BaseSettings):
    """Settings for base currency."""

    code: CurrencyCode
    name: str

    class Config:
        env_prefix = "base_currency_"
        case_sensitive = False


class Config(BaseSettings):
    postgres: PostgresSettings
    base_currency: BaseCurrencySettings
    jwt_secret: str
    jwt_algorithm: str = "HS256"
    jwt_expire_min: PositiveInt = 1440

    class Config:
        case_sensitive = False


def get_config() -> Config:
    env = os.getenv("ENVIRONMENT")
    if env is None:
        raise RuntimeError("Provide ENVIRONMENT env var")

    if env == "test":
        return Config(
            jwt_secret="very_secret",
            jwt_algorithm="HS256",
            jwt_expire_min=1440,
            postgres=PostgresSettings(
                driver="postgresql",
                host="127.0.0.1",
                port=5432,
                db="db",
                user="user",
                password="pass",
            ),
            base_currency=BaseCurrencySettings(
                code=CurrencyCode("rub"),
                name="Российский рубль",
            ),
        )

    return Config(
        postgres=PostgresSettings(),
        base_currency=BaseCurrencySettings(),
    )


config = get_config()
