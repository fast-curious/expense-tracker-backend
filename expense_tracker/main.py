from datetime import datetime, timedelta

from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.schedulers.background import BackgroundScheduler
from fastapi import FastAPI
from loguru import logger
from starlette_prometheus import PrometheusMiddleware, metrics

from expense_tracker.config import config
from expense_tracker.routers import (
    currencies_router,
    ping_router,
    tag_router,
    transactions_router,
    user_router,
)
from expense_tracker.security import security_router
from expense_tracker.tasks.exchange_rate import (
    populate_currencies_from_cbr_task,
    update_exchange_rates_task,
)

job_stores = {
    "default": SQLAlchemyJobStore(url=config.postgres.dsn),
}
scheduler = BackgroundScheduler(jobstores=job_stores)
app = FastAPI()
app.add_middleware(PrometheusMiddleware)
app.add_route("/metrics", metrics)

app.include_router(
    ping_router,
    prefix="/ping",
)
app.include_router(
    security_router,
)
app.include_router(
    user_router,
    prefix="/user",
)
app.include_router(
    transactions_router,
    prefix="/transactions",
)
app.include_router(
    tag_router,
    prefix="/tags",
)
app.include_router(
    currencies_router,
    prefix="/currencies",
)


@app.on_event("startup")
def run_background_tasks() -> None:
    logger.info("Scheduling tasks...")
    scheduler.add_job(
        populate_currencies_from_cbr_task,
        "interval",
        days=1,
        next_run_time=datetime.now() + timedelta(seconds=1),
    )
    scheduler.add_job(
        update_exchange_rates_task,
        "interval",
        hours=1,
        next_run_time=datetime.now() + timedelta(seconds=10),
    )
    scheduler.start()
    logger.info("Scheduler is running")


@app.on_event("shutdown")
def stop_background_tasks() -> None:
    if scheduler.running:
        logger.info("Stopping scheduler...")
        scheduler.shutdown()
