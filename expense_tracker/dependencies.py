from typing import Any, Optional, TypeVar

from fastapi import Depends
from sqlmodel import Session, SQLModel

from expense_tracker.config import config
from expense_tracker.db import get_session
from expense_tracker.exceptions import (
    CurrencyNotFound,
    NotFound,
    TagNotFound,
    TransactionNotFound,
)
from expense_tracker.models import Currency, CurrencyCode, Tag, Transaction, User
from expense_tracker.services.exchange_rate import add_base_currency

M = TypeVar("M", bound=SQLModel)


def get_existing_object(
    session: Session,
    object_mapper: type[M],
    object_id: Any,
    not_found_exception: type[NotFound],
    user: Optional[User] = None,
) -> M:
    db_object = session.get(object_mapper, object_id)
    if not db_object:
        raise not_found_exception()

    if not user:
        return db_object

    if hasattr(db_object, "user"):
        if db_object.user != user:  # type: ignore
            raise not_found_exception()
    else:
        raise RuntimeError(
            f"Mapper {object_mapper} has no attribute user, but validation was requested",
        )

    return db_object


def get_existing_transaction(
    session: Session, transaction_id: int, user: User
) -> Transaction:
    return get_existing_object(
        session,
        object_mapper=Transaction,
        object_id=transaction_id,
        not_found_exception=TransactionNotFound,
        user=user,
    )


def get_existing_currency(session: Session, currency_code: CurrencyCode) -> Currency:
    return get_existing_object(
        session,
        object_mapper=Currency,
        object_id=currency_code,
        not_found_exception=CurrencyNotFound,
    )


def get_existing_tag(session: Session, tag_id: int, user: User) -> Tag:
    return get_existing_object(
        session,
        object_mapper=Tag,
        object_id=tag_id,
        not_found_exception=TagNotFound,
        user=user,
    )


def get_base_currency(session: Session = Depends(get_session)) -> Currency:
    base_currency = session.get(Currency, config.base_currency.code)
    if not base_currency:
        base_currency = add_base_currency(session)
    return base_currency
