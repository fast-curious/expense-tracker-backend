from pydantic import BaseModel

from expense_tracker.models.user import Username


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Username
