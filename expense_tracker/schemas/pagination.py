from dataclasses import dataclass

from fastapi import Query
from pydantic import NonNegativeInt, PositiveInt


@dataclass
class LimitOffsetPagination:
    limit: PositiveInt = Query(default=30, lte=100)
    offset: NonNegativeInt = Query(default=0)
