from abc import ABC, abstractmethod

from fastapi import HTTPException, status


class NotFound(HTTPException, ABC):
    status_code: int = status.HTTP_404_NOT_FOUND

    @property
    @abstractmethod
    def object_name(self) -> str:
        pass

    def __init__(self) -> None:
        super().__init__(
            status_code=self.status_code,
            detail=f"{self.object_name.capitalize()} not found",
        )


class TransactionNotFound(NotFound):
    object_name: str = "Transaction"


class CurrencyNotFound(NotFound):
    object_name: str = "Currency"


class TagNotFound(NotFound):
    object_name: str = "Tag"
