from typing import Optional

from datetime import datetime

from sqlmodel import Session, select
from tenacity import Retrying, retry_if_exception_type, stop_after_attempt, wait_fixed

from expense_tracker.config import config
from expense_tracker.models import Currency, CurrencyCode, CurrencyDecimal, ExchangeRate
from expense_tracker.services.cbr import CBRClient, CBRError, CurrencyCBR

NUM_ATTEMPTS = 3  # max number of attempts to get currency rates from CBR
DELAY = 60  # delay between requests, in seconds


def get_exchange_rates() -> list[CurrencyCBR]:
    cbr_client = CBRClient()
    for attempt in Retrying(
        stop=stop_after_attempt(NUM_ATTEMPTS),
        wait=wait_fixed(DELAY),
        retry=retry_if_exception_type(CBRError),
        reraise=True,
    ):
        with attempt:
            return list(cbr_client.get_currency_rates().currency_rates.values())
    return []


def update_exchange_rates(session: Session) -> None:
    cbr_response = get_exchange_rates()
    base_currency = session.get(Currency, config.base_currency.code)
    if not base_currency:
        base_currency = add_base_currency(session)
    for cbr_currency in cbr_response:
        currency = session.get(Currency, cbr_currency.char_code.lower())
        if not currency:
            continue
        exchange_rate = session.exec(
            select(ExchangeRate).where(
                ExchangeRate.base_currency == base_currency,
                ExchangeRate.currency == currency,
            )
        ).first()
        if not exchange_rate:
            exchange_rate = ExchangeRate(
                currency=currency,
                base_currency=base_currency,
                nominal=cbr_currency.nominal,
                value=CurrencyDecimal(cbr_currency.value),
            )
        else:
            exchange_rate.nominal = cbr_currency.nominal
            exchange_rate.value = CurrencyDecimal(cbr_currency.value)
            exchange_rate.updated_at = datetime.utcnow()
        session.add(exchange_rate)
        session.commit()


def add_base_currency(session: Session) -> Currency:
    currency = Currency(
        code=config.base_currency.code,
        name=config.base_currency.name,
    )
    session.add(currency)
    session.commit()
    return currency


def populate_currencies_from_cbr(session: Session) -> None:
    currency_rates = get_exchange_rates()
    for cbr_currency in currency_rates:
        currency = session.get(Currency, cbr_currency.char_code.lower())
        if currency is None:
            currency = Currency(
                code=CurrencyCode(cbr_currency.char_code.lower()),
                name=cbr_currency.name,
            )
            session.add(currency)
            session.commit()


def get_amount_in_base_currency(
    session: Session, amount: CurrencyDecimal, currency_code: CurrencyCode
) -> Optional[CurrencyDecimal]:
    if currency_code == config.base_currency.code:
        return amount

    exchange_rate = session.exec(
        select(ExchangeRate).where(
            ExchangeRate.base_currency_code == config.base_currency.code,
            ExchangeRate.currency_code == currency_code,
        )
    ).first()

    if exchange_rate:
        return CurrencyDecimal(amount * exchange_rate.value / exchange_rate.nominal)
    return None
