from __future__ import annotations

from datetime import datetime
from decimal import Decimal

from pydantic import BaseModel, Field

__all__ = [
    "CurrencyCBR",
    "DailyRatesCBR",
]


class CurrencyCBR(BaseModel):
    id_: str = Field(..., alias="ID")
    num_code: int = Field(..., alias="NumCode")
    char_code: str = Field(..., alias="CharCode")
    nominal: int = Field(..., alias="Nominal")
    name: str = Field(..., alias="Name")
    value: Decimal = Field(..., alias="Value")
    previous: float = Field(..., alias="Previous")


class DailyRatesCBR(BaseModel):
    date_: str = Field(..., alias="Date")
    previous_date: datetime = Field(..., alias="PreviousDate")
    previous_url: str = Field(..., alias="PreviousURL")
    timestamp: datetime = Field(..., alias="Timestamp")
    currency_rates: dict[str, CurrencyCBR] = Field(..., alias="Valute")
