__all__ = [
    "CBRError",
    "CBRResponseError",
    "CBRConnectionError",
]


class CBRError(Exception):
    """Base exception for CBR API."""


class CBRConnectionError(CBRError):
    """Cannot fetch data from CBR API."""


class CBRResponseError(CBRError):
    """Invalid format of response from API."""
