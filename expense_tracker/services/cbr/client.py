import requests
from pydantic import ValidationError

from .exceptions import CBRConnectionError, CBRResponseError
from .models import DailyRatesCBR

__all__ = [
    "CBRClient",
]


class CBRClient:
    """
    Client for The Central Bank of Russian Federation.

    It uses 3rd party API which is more reliable and provides data in JSON:
    https://www.cbr-xml-daily.ru
    """

    CBR_DAILY_URL = "https://www.cbr-xml-daily.ru/daily_json.js"
    TIMEOUT = 5

    def get_currency_rates(self) -> DailyRatesCBR:
        try:
            response = requests.get(self.CBR_DAILY_URL, timeout=self.TIMEOUT)
            response.raise_for_status()
            return DailyRatesCBR.parse_raw(response.text)
        except requests.exceptions.HTTPError as e:
            raise CBRResponseError(str(e)) from e
        except requests.exceptions.RequestException as e:
            raise CBRConnectionError(str(e)) from e
        except ValidationError as e:
            raise CBRResponseError(str(e)) from e
