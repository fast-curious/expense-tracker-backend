from fastapi import APIRouter, Depends, Path
from sqlmodel import Session, select

from expense_tracker.db import get_session
from expense_tracker.dependencies import get_existing_tag
from expense_tracker.models import Tag, TagCreate, TagRead, TagUpdate, User
from expense_tracker.schemas.pagination import LimitOffsetPagination
from expense_tracker.security import get_current_user

router = APIRouter(
    tags=["Tag"],
)


def existing_tag(
    tag_id: int = Path(...),
    session: Session = Depends(get_session),
    user: User = Depends(get_current_user),
) -> Tag:
    return get_existing_tag(session, tag_id, user)


@router.get("/{tag_id}", response_model=TagRead)
def read_tag(
    db_tag: Tag = Depends(existing_tag),
) -> Tag:
    return db_tag


@router.get("", response_model=list[TagRead])
def read_tags(
    pagination: LimitOffsetPagination = Depends(),
    session: Session = Depends(get_session),
    user: User = Depends(get_current_user),
) -> list[Tag]:
    tags = session.exec(
        select(Tag)
        .where(Tag.user == user)
        .offset(pagination.offset)
        .limit(pagination.limit)
    ).all()
    return tags


@router.post("", response_model=TagRead)
def create_tag(
    tag: TagCreate,
    session: Session = Depends(get_session),
    user: User = Depends(get_current_user),
) -> Tag:
    db_tag = Tag(**tag.dict(), user=user)
    session.add(db_tag)
    session.commit()
    session.refresh(db_tag)
    return db_tag


@router.patch("/{tag_id}", response_model=TagRead)
def update_tag(
    tag: TagUpdate,
    session: Session = Depends(get_session),
    db_tag: Tag = Depends(existing_tag),
) -> Tag:
    tag_data = tag.dict(exclude_unset=True)
    for key, value in tag_data.items():
        setattr(db_tag, key, value)
    session.commit()
    session.refresh(db_tag)
    return db_tag


@router.delete("/{tag_id}", response_model=TagRead)
def delete_tag(
    db_tag: Tag = Depends(existing_tag),
    session: Session = Depends(get_session),
) -> Tag:
    session.delete(db_tag)
    session.commit()
    return db_tag
