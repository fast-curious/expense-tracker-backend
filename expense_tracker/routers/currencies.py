from fastapi import APIRouter, Depends
from sqlmodel import Session, select

from expense_tracker.db import get_session
from expense_tracker.models import Currency
from expense_tracker.schemas.pagination import LimitOffsetPagination

router = APIRouter(
    tags=["Currencies"],
)


@router.get("")
def list_currencies(
    pagination: LimitOffsetPagination = Depends(),
    session: Session = Depends(get_session),
) -> list[Currency]:
    currencies = session.exec(
        select(Currency).offset(pagination.offset).limit(pagination.limit)
    ).all()
    return currencies
