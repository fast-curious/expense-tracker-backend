from fastapi import APIRouter, Depends
from sqlmodel import Session

from expense_tracker.db import get_session
from expense_tracker.models import User, UserCreate, UserRead
from expense_tracker.security import get_current_user, hash_password

router = APIRouter(
    tags=["User"],  # pragma: no mutate
)


@router.post("", response_model=UserRead)
def create_user(
    new_user_data: UserCreate,
    session: Session = Depends(get_session),
) -> User:
    hashed_password = hash_password(new_user_data.password)
    user = User(
        username=new_user_data.username,
        password_hash=hashed_password,
    )
    session.add(user)
    session.commit()
    return user


@router.get("/me", response_model=UserRead)
async def get_me(
    user: User = Depends(get_current_user),
) -> User:
    return user
