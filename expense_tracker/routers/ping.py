from fastapi import APIRouter

router = APIRouter(
    tags=["Service"],
)


@router.get("")
def ping() -> dict[str, bool]:
    return {"ping": True}
