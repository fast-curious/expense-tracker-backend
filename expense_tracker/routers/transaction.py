from fastapi import APIRouter, Depends, Path
from sqlmodel import Session, col, select

from expense_tracker.db import get_session
from expense_tracker.dependencies import (
    get_base_currency,
    get_existing_currency,
    get_existing_tag,
    get_existing_transaction,
)
from expense_tracker.models import (
    Currency,
    Transaction,
    TransactionCreate,
    TransactionRead,
    TransactionUpdate,
    User,
)
from expense_tracker.schemas.pagination import LimitOffsetPagination
from expense_tracker.security import get_current_user
from expense_tracker.services.exchange_rate import get_amount_in_base_currency

router = APIRouter(
    tags=["Transaction"],  # pragma: no mutate
)


def existing_transaction(
    transaction_id: int = Path(...),
    session: Session = Depends(get_session),
    user: User = Depends(get_current_user),
) -> Transaction:
    return get_existing_transaction(session, transaction_id, user)


@router.get("/{transaction_id}", response_model=TransactionRead)
def read_transaction(
    db_transaction: Transaction = Depends(existing_transaction),
) -> Transaction:
    return db_transaction


@router.get("", response_model=list[TransactionRead])
def read_transactions(
    pagination: LimitOffsetPagination = Depends(),
    session: Session = Depends(get_session),
    user: User = Depends(get_current_user),
) -> list[Transaction]:
    transactions = session.exec(
        select(Transaction)
        .where(Transaction.user == user)
        .order_by(
            col(Transaction.created_at).desc(),
            col(Transaction.id).desc(),
        )
        .offset(pagination.offset)
        .limit(pagination.limit)
    ).all()
    return transactions


@router.post("", response_model=TransactionRead)
def create_transaction(
    transaction: TransactionCreate,
    session: Session = Depends(get_session),
    user: User = Depends(get_current_user),
    base_currency: Currency = Depends(get_base_currency),
) -> Transaction:
    get_existing_currency(session, transaction.currency_code)

    if transaction.tag_id is not None:
        get_existing_tag(session, transaction.tag_id, user)

    base_amount = get_amount_in_base_currency(
        session, transaction.amount, transaction.currency_code
    )
    db_transaction = Transaction(
        **transaction.dict(),
        user=user,
        base_currency=base_currency,
        base_amount=base_amount,
    )
    session.add(db_transaction)
    session.commit()
    session.refresh(db_transaction)
    return db_transaction


@router.patch("/{transaction_id}", response_model=TransactionRead)
def update_transaction(
    transaction: TransactionUpdate,
    db_transaction: Transaction = Depends(existing_transaction),
    session: Session = Depends(get_session),
    user: User = Depends(get_current_user),
) -> Transaction:

    if transaction.currency_code is not None:
        get_existing_currency(session, transaction.currency_code)

    if transaction.tag_id is not None:
        get_existing_tag(session, transaction.tag_id, user)

    transaction_data = transaction.dict(exclude_unset=True)
    for key, value in transaction_data.items():
        setattr(db_transaction, key, value)

    if transaction.amount is not None:
        currency_code = transaction.currency_code or db_transaction.currency_code
        db_transaction.base_amount = get_amount_in_base_currency(
            session, transaction.amount, currency_code
        )

    session.add(db_transaction)
    session.commit()
    session.refresh(db_transaction)
    return db_transaction


@router.delete("/{transaction_id}", response_model=TransactionRead)
def delete_transaction(
    session: Session = Depends(get_session),
    db_transaction: Transaction = Depends(existing_transaction),
) -> Transaction:
    session.delete(db_transaction)
    session.commit()
    return db_transaction
