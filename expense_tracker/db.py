from typing import Iterator

from sqlmodel import Session, create_engine

from expense_tracker.config import config

engine = create_engine(config.postgres.dsn)


def get_session() -> Iterator[Session]:
    with Session(engine) as session:
        yield session
