from typing import Optional

from datetime import datetime, timedelta

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from passlib.hash import bcrypt
from sqlmodel import Session

from expense_tracker import models
from expense_tracker.config import config
from expense_tracker.db import get_session
from expense_tracker.schemas.token import Token, TokenData

token_url = "token"
oauth2_scheme = OAuth2PasswordBearer(tokenUrl=token_url)
security_router = APIRouter(
    prefix="",
    tags=["Security"],
)


def verify_password(password: str, hashed_password: str) -> bool:
    return bcrypt.verify(password, hashed_password)


def hash_password(password: str) -> str:
    return bcrypt.hash(password)


def create_access_token(
    data: TokenData,
    expires_delta: Optional[timedelta] = None,
) -> str:
    to_encode = data.dict()
    if expires_delta is not None:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=config.jwt_expire_min)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(
        to_encode,
        config.jwt_secret,
        algorithm=config.jwt_algorithm,
    )
    return encoded_jwt


def authenticate_user(
    session: Session, username: str, password: str
) -> Optional[models.User]:
    user = session.get(models.User, username)
    if user is None:
        return None
    if not verify_password(password, user.password_hash):
        return None
    return user


def get_current_user(
    session: Session = Depends(get_session),
    token: str = Depends(oauth2_scheme),
) -> models.User:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(
            token,
            config.jwt_secret,
            algorithms=[config.jwt_algorithm],
        )
        username: models.user.Username = payload.get("username")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError as e:
        raise credentials_exception from e
    user = session.get(models.User, token_data.username)
    if user is None:
        raise credentials_exception
    return user


@security_router.post(f"/{token_url}", response_model=Token)
def generate_token(
    form_data: OAuth2PasswordRequestForm = Depends(),
    session: Session = Depends(get_session),
) -> Token:
    user = authenticate_user(
        session,
        form_data.username,
        form_data.password,
    )

    if user is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    access_token = create_access_token(TokenData(username=user.username))
    return Token(access_token=access_token, token_type="bearer")
